1. Create a virtual environment `python -m venv .venv`
2. Activate the virtual environment `source .venv/bin/activate`
3. Install requirements `pip install -r requirements.txt`
4. Run migrations `python manage.py migrate`
5. Load data `python manage.py loaddata demonstration/data.json`
6. Start your app server `python manage.py runserver`